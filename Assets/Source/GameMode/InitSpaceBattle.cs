using Source.ECS.Components;
using Source.ECS.Components.Tags;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;


namespace Source.GameMode
{
    public class InitSpaceBattle : MonoBehaviour
    {
        private EntityManager _entityManager;
        private PrefabList _prefabList;

        public int blueTeamCount = 10;
        public int redTeamCount = 10;

        private Entity arenaEntity;

        void Start()
        {
            _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            _prefabList = GetComponent<PrefabList>();
            _prefabList.Init();

            CreateArena();
            CreateShips();
        }

        private void CreateArena()
        {
            arenaEntity = _entityManager.Instantiate(_prefabList.ArenaPrefabEntity);
        }

        void CreateShips()
        {
            var shipQuery = _entityManager.CreateEntityQuery(typeof(ShipTag));
            var arenaData = _entityManager.GetComponentData<ArenaData>(arenaEntity);
            
            var minRadius = (float)(0.5 * arenaData.Radius);
            var maxRadius = (float)(0.9 * arenaData.Radius);
            var minAlpha = 45;
            var maxAlpha = 135;
            var minBeta = -45;
            var maxBeta = 45;

            var random = Random.CreateFromIndex(0);
            
            var redShips = _entityManager.Instantiate(_prefabList.RedShipPrefabEntity, redTeamCount, Allocator.Persistent);
            foreach(var ship in redShips)
            {
                var translation = new Translation();
                var shperCoord = random.NextFloat3(new float3(minRadius, minAlpha, minBeta), new float3(maxRadius, maxAlpha, maxBeta));
                translation.Value = new float3
                {
                    x = shperCoord.x * Mathf.Sin(shperCoord.y * Mathf.Deg2Rad) * Mathf.Cos(shperCoord.z * Mathf.Deg2Rad),
                    z = shperCoord.x * Mathf.Sin(shperCoord.y * Mathf.Deg2Rad) * Mathf.Sin(shperCoord.z * Mathf.Deg2Rad),
                    y = shperCoord.x * Mathf.Cos(shperCoord.y * Mathf.Deg2Rad),
                };
                
                var rotation = new Rotation();
                rotation.Value = Quaternion.AngleAxis(270, Vector3.right);
                rotation.Value *= Quaternion.AngleAxis(270, Vector3.forward);
                
                _entityManager.SetComponentData(ship, translation);
                _entityManager.SetComponentData(ship, rotation);

                var weapon = _entityManager.GetComponentData<Weapon>(ship);
                weapon.CurrentCooldown = random.NextFloat(0, weapon.Cooldown);
                _entityManager.SetComponentData(ship, weapon);
            }
            redShips.Dispose();
            
            minAlpha += 180;
            maxAlpha += 180;
            
            var blueShips = _entityManager.Instantiate(_prefabList.BlueShipPrefabEntity, blueTeamCount, Allocator.Persistent);
            foreach(var ship in blueShips)
            {
                var translation = new Translation();
                var shperCoord = random.NextFloat3(new float3(minRadius, minAlpha, minBeta), new float3(maxRadius, maxAlpha, maxBeta));
                translation.Value = new float3
                {
                    x = shperCoord.x * Mathf.Sin(shperCoord.y * Mathf.Deg2Rad) * Mathf.Cos(shperCoord.z * Mathf.Deg2Rad),
                    z = shperCoord.x * Mathf.Sin(shperCoord.y * Mathf.Deg2Rad) * Mathf.Sin(shperCoord.z * Mathf.Deg2Rad),
                    y = shperCoord.x * Mathf.Cos(shperCoord.y * Mathf.Deg2Rad),
                };
                
                var rotation = new Rotation();
                rotation.Value = Quaternion.AngleAxis(270, Vector3.right);
                rotation.Value *= Quaternion.AngleAxis(90, Vector3.forward);
                
                _entityManager.SetComponentData(ship, translation);
                _entityManager.SetComponentData(ship, rotation);
                
                var weapon = _entityManager.GetComponentData<Weapon>(ship);
                weapon.CurrentCooldown = random.NextFloat(0, weapon.Cooldown);
                _entityManager.SetComponentData(ship, weapon);
            }
            blueShips.Dispose();
            
            var shipSharedData = new ShipSharedData
            {
                BulletPrefabEntity = _prefabList.BulletPrefabEntity,
                ArenaEntity = arenaEntity
            };
            _entityManager.AddSharedComponentData(shipQuery, shipSharedData);
        }
    }
}
using System;
using Source.ECS.Components;
using Source.ECS.Components.Tags;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Serialization;

namespace Source.GameMode
{
    public class PrefabList : MonoBehaviour
    {
        private EntityManager _entityManager;
        private BlobAssetStore _blobAssetStore;
        private GameObjectConversionSettings _settings;

        public GameObject blueShipPrefab;
        public GameObject redShipPrefab;
        public GameObject arenaPrefab;
        
        public GameObject bulletPrefab;

        public Entity BlueShipPrefabEntity;
        public Entity RedShipPrefabEntity;
        public Entity ArenaPrefabEntity;
        public Entity BulletPrefabEntity;

        public void Init()
        {
            _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            _blobAssetStore = new BlobAssetStore();
            _settings = GameObjectConversionSettings.FromWorld(_entityManager.World, _blobAssetStore);
            
            BlueShipPrefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(blueShipPrefab, _settings);
            RedShipPrefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(redShipPrefab, _settings);
            ArenaPrefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(arenaPrefab, _settings);
            BulletPrefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, _settings);
        }

        public void OnDestroy()
        {
            //Debug.Log("Destroy blobAssetStore");
            _blobAssetStore.Dispose();
        }
    }
}
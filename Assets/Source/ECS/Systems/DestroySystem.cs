using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using static Unity.Mathematics.math;

namespace Source.ECS.Systems
{
    [UpdateAfter(typeof(MoveSystem))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class DestroySystem : SystemBase
    {
        [BurstCompile]
        struct DestroyJob : IJobEntityBatchWithIndex
        {
            [ReadOnly]
            public NativeArray<Entity> Entities;
            [ReadOnly]
            public ComponentTypeHandle<Translation> TranslationsHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<Target> TargetsHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<ShipTag> ShipTagHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<BulletTag> BulletTagHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<Damage> DamageHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<Velocity> VelocityHandleAccessor;

            [ReadOnly] 
            public ArenaData ArenaData;
            
            [ReadOnly]
            public ComponentDataFromEntity<Translation> TargetTranslations;
            
            [ReadOnly]
            public ComponentDataFromEntity<Health> TargetHealth;
            
            public EntityCommandBuffer.ParallelWriter BufferHandle;

            public void Execute(ArchetypeChunk batchInChunk, int batchIndex, int indexOfFirstEntityInQuery)
            {
                var translations = batchInChunk.GetNativeArray(TranslationsHandleAccessor);
                var targets = batchInChunk.GetNativeArray(TargetsHandleAccessor);
                var damages = batchInChunk.GetNativeArray(DamageHandleAccessor);
                var velocities = batchInChunk.GetNativeArray(VelocityHandleAccessor);

                for (int i = 0; i < translations.Length; i++)
                {
                    var translation = translations[i];
                    var target = targets[i];
                    
                    //Check out of arena
                    if (distance(translation.Value, ArenaData.Center) > ArenaData.Radius)
                    {
                        BufferHandle.DestroyEntity(batchIndex, Entities[indexOfFirstEntityInQuery + i]);
                        
                        //work if object is bullet
                        if (batchInChunk.Has(BulletTagHandleAccessor))
                        {
                            if(target.Value == Entity.Null)
                                continue;
                            
                            BufferHandle.DestroyEntity(batchIndex, Entities[indexOfFirstEntityInQuery + i]);
                            var health = TargetHealth[target.Value];
                            health.Value -= damages[i].Value;
                            
                            BufferHandle.DestroyEntity(batchIndex, Entities[indexOfFirstEntityInQuery + i]);
                            if (health.Value < 0)
                            {
                                BufferHandle.DestroyEntity(batchIndex, target.Value); 
                            }
                            else
                            {
                                BufferHandle.SetComponent(batchIndex, target.Value, health);
                            }
                        }
                        
                        continue;
                    }
                    
                    if(target.Value == Entity.Null)
                        continue;

                    var targetTranslation = TargetTranslations[target.Value];

                    //work if object is ship
                    if (batchInChunk.Has(ShipTagHandleAccessor))
                    {
                        if (distance(translation.Value, targetTranslation.Value) < 1)
                        {
                            BufferHandle.DestroyEntity(batchIndex, Entities[indexOfFirstEntityInQuery + i]);
                            BufferHandle.DestroyEntity(batchIndex, target.Value);
                        }
                        continue;
                    }
                }
            }
        }
        
        private EntityQuery _shipsQuery;
        private EntityQuery _bulletQuery;
        private EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

        protected override void OnCreate()
        {
            var queryDescription = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<ShipTag>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Target>(),
                    typeof(Health)
                }
            };

            _shipsQuery = GetEntityQuery(queryDescription);
            
            
            queryDescription = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<BulletTag>(),
                    ComponentType.ReadOnly<Target>(),
                    ComponentType.ReadOnly<Damage>(),
                }
            };
            _bulletQuery = GetEntityQuery(queryDescription);
            
            _endSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            if (_shipsQuery.IsEmpty && _bulletQuery.IsEmpty)
                return;
            
            var shipEntities = _shipsQuery.ToEntityArray(Allocator.TempJob);
            if (shipEntities.Length == 0)
                return;
            
            var shipSharedData = EntityManager.GetSharedComponentData<ShipSharedData>(shipEntities[0]);
            var arenaData = GetComponent<ArenaData>(shipSharedData.ArenaEntity);

            if (!_shipsQuery.IsEmpty)
            {
                var bullet2Ship = new DestroyJob
                {
                    Entities = shipEntities,
                    TranslationsHandleAccessor = GetComponentTypeHandle<Translation>(true),
                    TargetsHandleAccessor = GetComponentTypeHandle<Target>(true),
                    ShipTagHandleAccessor = GetComponentTypeHandle<ShipTag>(true),
                    BulletTagHandleAccessor = GetComponentTypeHandle<BulletTag>(true),
                    DamageHandleAccessor = GetComponentTypeHandle<Damage>(true),
                    VelocityHandleAccessor = GetComponentTypeHandle<Velocity>(true),
                    ArenaData = arenaData,
                    TargetTranslations = GetComponentDataFromEntity<Translation>(true),
                    TargetHealth = GetComponentDataFromEntity<Health>(false),
                    BufferHandle = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter()
                };
            
                var handle = bullet2Ship.ScheduleParallel(_shipsQuery, 1, Dependency);
                handle.Complete();
            }
            shipEntities.Dispose();

            if (!_bulletQuery.IsEmpty)
            {
                var bulletEntities = _bulletQuery.ToEntityArray(Allocator.TempJob);
                var ship2Ship = new DestroyJob
                {
                    Entities = bulletEntities,
                    TranslationsHandleAccessor = GetComponentTypeHandle<Translation>(true),
                    TargetsHandleAccessor = GetComponentTypeHandle<Target>(true),
                    ShipTagHandleAccessor = GetComponentTypeHandle<ShipTag>(true),
                    BulletTagHandleAccessor = GetComponentTypeHandle<BulletTag>(true),
                    DamageHandleAccessor = GetComponentTypeHandle<Damage>(true),
                    VelocityHandleAccessor = GetComponentTypeHandle<Velocity>(true),
                    ArenaData = arenaData,
                    TargetTranslations = GetComponentDataFromEntity<Translation>(true),
                    TargetHealth = GetComponentDataFromEntity<Health>(false),
                    BufferHandle = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter()
                };

                var handle = ship2Ship.ScheduleParallel(_bulletQuery, 1, Dependency);
                handle.Complete();
                bulletEntities.Dispose();
            }
        }
    }
}
using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Source.ECS.Systems
{
    [UpdateBefore(typeof(MoveSystem))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class AttackSystem : SystemBase
    {
        [BurstCompile]
        struct AttackJob : IJobEntityBatch
        {
            [ReadOnly]
            public ComponentTypeHandle<Translation> TranslationsHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<MoveDirection> MoveDirectionHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<Target> TargetHandleAccessor;
            
            public ComponentTypeHandle<Weapon> WeaponHandleAccessor;
            
            [ReadOnly]
            public Entity BulletPrefabEntity;
            
            public EntityCommandBuffer.ParallelWriter BufferHandle;

            [ReadOnly]
            public float DeltaTime;

            public void Execute(ArchetypeChunk batchInChunk, int batchIndex)
            {
                var translations = batchInChunk.GetNativeArray(TranslationsHandleAccessor);
                var moveDirections = batchInChunk.GetNativeArray(MoveDirectionHandleAccessor);
                var weapons = batchInChunk.GetNativeArray(WeaponHandleAccessor);
                var targets = batchInChunk.GetNativeArray(TargetHandleAccessor);

                for (int i = 0; i < translations.Length; i++)
                {
                    if(targets[i].Value == Entity.Null)
                        continue;
                    
                    var weapon = weapons[i];
                    if (weapon.CurrentCooldown > 0)
                    {
                        weapon.CurrentCooldown -= DeltaTime;
                        weapons[i] = weapon;
                        continue;
                    }
                    
                    var entity = BufferHandle.Instantiate(batchIndex, BulletPrefabEntity);
                    var damage = new Damage
                    {
                        Value = weapon.Damage
                    };
                    var velocity = new Velocity
                    {
                        Speed = weapon.ProjectileSpeed,
                        MaxSpeed = weapon.ProjectileSpeed
                    };
                    var rotation = new Rotation
                    {
                        Value = Quaternion.LookRotation(moveDirections[i].Value, Vector3.up)
                    };

                    BufferHandle.AddComponent(batchIndex, entity, translations[i]);
                    BufferHandle.AddComponent(batchIndex, entity, moveDirections[i]);
                    BufferHandle.AddComponent(batchIndex, entity, damage);
                    BufferHandle.AddComponent(batchIndex, entity, velocity);
                    BufferHandle.AddComponent(batchIndex, entity, targets[i]);
                    BufferHandle.AddComponent(batchIndex, entity, rotation);

                    weapon.CurrentCooldown = weapon.Cooldown;
                    weapons[i] = weapon;
                }
            }
        }
        
        private EntityQuery _shipsQuery;
        private EntityQuery _bulletQuery;
        private EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

        protected override void OnCreate()
        {
            var queryDescription = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<ShipTag>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<MoveDirection>(),
                    ComponentType.ReadOnly<Target>(),
                    typeof(Weapon)
                }
            };

            _shipsQuery = GetEntityQuery(queryDescription);
            
            
            queryDescription = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<BulletTag>(),
                }
            };
            _bulletQuery = GetEntityQuery(queryDescription);
            
            _endSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            if (_shipsQuery.IsEmpty)
                return;

            var shipCount = _shipsQuery.CalculateEntityCount();
            var bulletCount = _bulletQuery.CalculateEntityCount();
            
            if (bulletCount > 10 * shipCount)
                return;

            var entities = _shipsQuery.ToEntityArray(Allocator.Temp);
            var shipSharedData = EntityManager.GetSharedComponentData<ShipSharedData>(entities[0]);
            entities.Dispose();
            
            var job = new AttackJob
            {
                TranslationsHandleAccessor = GetComponentTypeHandle<Translation>(true),
                MoveDirectionHandleAccessor = GetComponentTypeHandle<MoveDirection>(true),
                TargetHandleAccessor = GetComponentTypeHandle<Target>(true),
                WeaponHandleAccessor = GetComponentTypeHandle<Weapon>(false),
                BulletPrefabEntity = shipSharedData.BulletPrefabEntity,
                BufferHandle = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter(),
                DeltaTime = Time.DeltaTime
            };

            var handle = job.ScheduleParallel(_shipsQuery, 1, Dependency);
            handle.Complete();
        }
    }
}
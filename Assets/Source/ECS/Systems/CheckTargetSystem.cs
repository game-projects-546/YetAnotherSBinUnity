using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using static Unity.Mathematics.math;

namespace Source.ECS.Systems
{
    [UpdateBefore(typeof(FindTarget))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class CheckTargetSystem : SystemBase
    {

        protected override void OnUpdate()
        {
            Entities.WithAny<ShipTag, BulletTag>()
                .ForEach(
                    (ref Target target) =>
                    {
                        if (!HasComponent<ShipTag>(target.Value))
                        {
                            target.Value = Entity.Null;
                        }
                    })
                .WithBurst()
                .ScheduleParallel();
        }
    }
}
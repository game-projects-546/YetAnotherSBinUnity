using System.Net;
using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Source.GameMode;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using static Unity.Mathematics.math;
using quaternion = Unity.Mathematics.quaternion;

namespace Source.ECS.Systems
{
    [UpdateBefore(typeof(SetDirectionSystem))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class FindTarget : SystemBase
    {
        [BurstCompile]
        struct FindTargetJob : IJobEntityBatch
        {
            [ReadOnly]
            public ComponentTypeHandle<Team> TeamsHandleAccessor;
            [ReadOnly]
            public ComponentTypeHandle<Translation> TranslationsHandleAccessor;
            
            public ComponentTypeHandle<Target> TargetsHandleAccessor;
            
            
            [ReadOnly]
            public NativeArray<Entity> Entities;
            [ReadOnly]
            public ComponentDataFromEntity<Team> TargetTeams;
            [ReadOnly]
            public ComponentDataFromEntity<Translation> TargetTranslations;

            public void Execute(ArchetypeChunk batchInChunk, int batchIndex)
            {
                var teams = batchInChunk.GetNativeArray(TeamsHandleAccessor);
                var translations = batchInChunk.GetNativeArray(TranslationsHandleAccessor);
                var targets = batchInChunk.GetNativeArray(TargetsHandleAccessor);

                for (int i = 0; i < teams.Length; i++)
                {
                    if (targets[i].Value != Entity.Null)
                        return;
                    
                    float dist = float.MaxValue;
                    Entity targetValue = Entity.Null;
                    for (int j = 0; j < Entities.Length; j++)
                    {
                        var targetTeam = TargetTeams[Entities[j]];
                        if(targetTeam.Value == teams[i].Value)
                            continue;
                            
                        var targetTranslation = TargetTranslations[Entities[j]];
                        var newDist = distance(translations[i].Value, targetTranslation.Value);
                        if (newDist < dist)
                        {
                            dist = newDist;
                            targetValue = Entities[j];
                        }
                        
                    }
                    
                    targets[i] = new Target{
                        Value = targetValue
                    };
                }
            }
        }
        
        private EntityQuery _shipsQuery;

        protected override void OnCreate()
        {
            var queryDescription = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<ShipTag>(),
                    ComponentType.ReadOnly<Team>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Target>()
                }
            };

            _shipsQuery = GetEntityQuery(queryDescription);

            base.OnCreate();
        }
        
        ///*
        protected override void OnUpdate()
        {
            if (_shipsQuery.IsEmpty)
                return;
            
            var entities = _shipsQuery.ToEntityArray(Allocator.TempJob);

            var job = new FindTargetJob
            {
                TeamsHandleAccessor = GetComponentTypeHandle<Team>(true),
                TranslationsHandleAccessor = GetComponentTypeHandle<Translation>(true),
                TargetsHandleAccessor = GetComponentTypeHandle<Target>(false),
                Entities = entities,
                TargetTeams = GetComponentDataFromEntity<Team>(true),
                TargetTranslations = GetComponentDataFromEntity<Translation>(true),
            };

            var handle = job.ScheduleParallel(_shipsQuery, 1, this.Dependency);
            handle.Complete();

            entities.Dispose();
        }
        //*/
        /*
        protected override void OnUpdate()
        {
            if (_shipsQuery.IsEmpty)
                return;

            var entities = _shipsQuery.ToEntityArray(Allocator.Temp);            

            Entities.WithAll<ShipTag>().ForEach(
                    (Entity entity, ref Target target, in Team team, in Translation translation) =>
                    {
                        if (target.Value != Entity.Null)
                            return;
                        
                        float dist = float.MaxValue;
                        Entity targetValue = Entity.Null;
                        for (int i = 0; i < entities.Length; i++)
                        {
                            var targetTeam = GetComponent<Team>(entities[i]);
                            if(targetTeam.Value == team.Value)
                                continue;
                            
                            var targetTranslation = GetComponent<Translation>(entities[i]);
                            var newDist = distance(translation.Value, targetTranslation.Value);
                            if (newDist < dist)
                            {
                                dist = newDist;
                                targetValue = entities[i];
                            }
                        }

                        target.Value = targetValue;
                    })
                //.WithBurst()
                .WithoutBurst()
                .Run();

            entities.Dispose();
        }
        */
    }
}
using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace Source.ECS.Systems
{
    [UpdateBefore(typeof(MoveSystem))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class SetDirectionSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var deltaTime = UnityEngine.Time.deltaTime;

            Entities.WithAll<Target, ShipTag>()
                .ForEach(
                    (Entity entity, ref MoveDirection moveDirection, in Translation translation, in Target target) =>
                    {
                        if (target.Value == Entity.Null)
                            return;

                        var targetTranslation = GetComponent<Translation>(target.Value);
                        var direction = targetTranslation.Value - translation.Value;
                        moveDirection.Value = normalize(direction);
                    })
                .WithBurst()
                .ScheduleParallel();
        }
    }
}
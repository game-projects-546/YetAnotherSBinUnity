using Source.ECS.Components;
using Source.ECS.Components.Ships;
using Source.ECS.Components.Tags;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using static Unity.Mathematics.math;

namespace Source.ECS.Systems
{
    [UpdateAfter(typeof(FindTarget))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class MoveSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var deltaTime = UnityEngine.Time.deltaTime;

            Entities
                .ForEach(
                    (ref Translation translation, ref Rotation rotation, ref Velocity velocity,
                        in MoveDirection direction) =>
                    {
                        
                        var newVelDir = normalize(lerp(direction.Value, velocity.Direction, 0.5));
                        velocity.Direction = (float3) newVelDir;

                        var newVelForce = lerp(velocity.Speed, velocity.MaxSpeed, 0.1);
                        velocity.Speed = (float) newVelForce;

                        translation.Value += velocity.Direction * velocity.Speed * deltaTime;
                    })
                .WithBurst()
                .ScheduleParallel();
        }
    }
}
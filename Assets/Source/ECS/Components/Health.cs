using System;
using Unity.Entities;

namespace Source.ECS.Components
{
    
    [GenerateAuthoringComponent, Serializable]
    public struct Health : IComponentData
    {
        public float Value;
    }
}

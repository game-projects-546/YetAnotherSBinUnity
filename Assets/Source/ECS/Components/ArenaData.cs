using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Source.ECS.Components
{
    [GenerateAuthoringComponent, Serializable]
    public struct ArenaData : IComponentData
    {
        public float3 Center;
        public float Radius;

    }
}

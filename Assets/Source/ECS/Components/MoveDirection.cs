using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Source.ECS.Components
{
    [GenerateAuthoringComponent, Serializable]
    public struct MoveDirection : IComponentData
    {
        public float3 Value;
    }
}

using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Source.ECS.Components
{
    [GenerateAuthoringComponent, Serializable]
    public struct Damage : IComponentData
    {
        public float Value;
    }
}

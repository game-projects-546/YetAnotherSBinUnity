using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Source.ECS.Components.Ships
{
    public enum ShipTeam
    {
        Red,
        Blue
    }
    
    [GenerateAuthoringComponent, Serializable]
    public struct Team : IComponentData
    {
        public ShipTeam Value;
    }
}

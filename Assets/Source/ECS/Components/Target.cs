using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Source.ECS.Components.Ships
{
    [GenerateAuthoringComponent, Serializable]
    public struct Target : IComponentData
    {
        public Entity Value;
    }
}

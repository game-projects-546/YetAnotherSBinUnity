using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Source.ECS.Components
{
    [GenerateAuthoringComponent, Serializable]
    public struct Weapon : IComponentData
    {
        public float Cooldown;
        public float CurrentCooldown;
        public float Damage;
        public float ProjectileSpeed;
    }
}

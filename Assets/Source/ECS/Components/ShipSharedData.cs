using System;
using Unity.Entities;

namespace Source.ECS.Components
{
    [Serializable]
    public struct ShipSharedData : ISharedComponentData, IEquatable <ShipSharedData>
    {
        public Entity BulletPrefabEntity;
        public Entity ArenaEntity;

        public bool Equals(ShipSharedData other)
        {
            return BulletPrefabEntity.Equals(other.BulletPrefabEntity) && ArenaEntity.Equals(other.ArenaEntity);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (BulletPrefabEntity.GetHashCode() * 397) ^ ArenaEntity.GetHashCode();
            }
        }
    }
}

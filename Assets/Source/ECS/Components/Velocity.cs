using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

namespace Source.ECS.Components
{
    [GenerateAuthoringComponent, Serializable]
    public struct Velocity : IComponentData
    {
        public float3 Direction;
        public float Speed;
        public float MaxSpeed;
    }
}
